package com.eftcorp;

import java.io.IOException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class FindAtm extends HttpServlet		
{
	public static String toPrettyFormat(String jsonString) 
	  {
	      JsonParser parser = new JsonParser();
	      JsonObject json = parser.parse(jsonString).getAsJsonObject();

	      Gson gson = new GsonBuilder().setPrettyPrinting().create();
	      String prettyJson = gson.toJson(json);

	      return prettyJson;
	  }
	
	public void service(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		PrintWriter out = res.getWriter();
		
		try
		{
			
			
			String cityname = req.getParameter("city");
			String postal_code = req.getParameter("postal_code");
			String geoLocation = req.getParameter("geolocation");
			String address = req.getParameter("address");
			
			Locate l1 = new Locate();
			l1.setCityName(cityname);
			l1.setAddress(address);
			l1.setLocation(geoLocation);
			l1.setPostalCode(postal_code);
			
//			 Gson json = new Gson();
//			 
//			String response = json.toJson(l1);
//			JSONObject son = new JSONObject(response);
			
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String response = gson.toJson(l1);
		    out.println(response);
			
		}
		catch(IllegalArgumentException i)
		{
			
	        out.println("Invalid input type please enter text");
		}
		
	}
}
