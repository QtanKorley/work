package com.eftcorp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.URL;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;

import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

public class ShowAtm extends HttpServlet		
{
	public String getRemoteContents(String url) throws Exception {
	    URL urlObject = new URL(url);
	    java.net.URLConnection conn = urlObject.openConnection();
	    BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    String inputLine, output = "";
	    while ((inputLine = in.readLine()) != null) {
	         output += inputLine;
	    }   
	    in.close();
	        
	    return output;
	}
	
	public void service(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		PrintWriter out = res.getWriter();
		
		try
		{
			
			
			String cityname = req.getParameter("city_name");
			try
			{
				
				// client
				Client  client = ClientBuilder.newClient(); 
				
				//Target
				WebTarget target = client.target("https://apis-bank-dev.apigee.net/apis/v1/locations/atms").path("name");
				
				Invocation.Builder invocationBuilder =  target.request(MediaType.APPLICATION_XML);
				Response response = invocationBuilder.get();
				
				String result = response.readEntity(String.class);
				
				out.println(result);
				

				
			} 
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				out.println("Euo");
			}
			
			
		}
		catch(IllegalArgumentException i)
		{
			
	        out.println("Invalid input type please enter text");
		}
		
	}
}

