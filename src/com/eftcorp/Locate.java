package com.eftcorp;

public class Locate {
	
	private String cityname;
	private String address;
	private String geolocation;
	private String postal_code;
	
	public Locate(String name) {
		this.cityname=name;
	}
	
	public Locate() {
		
	}
	
	public String getCityName()
	{
		return this.cityname;
	}
	public void setCityName(String name) 
	{
		this.cityname = name;
	}
	
	public String getPostalCode()
	{
		return this.postal_code;
	}
	public void setPostalCode(String code) 
	{
		this.postal_code = code;
	}
	
	public String getAddress()
	{
		return this.address;
	}
	public void setAddress(String address) 
	{
		this.address = address;
	}
	
	public String getLocation()
	{
		return this.geolocation;
	}
	public void setLocation(String location) 
	{
		this.geolocation = location;
	}
	
}
